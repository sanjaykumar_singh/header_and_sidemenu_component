import { HeaderDemoPage } from './app.po';

describe('header-demo App', () => {
  let page: HeaderDemoPage;

  beforeEach(() => {
    page = new HeaderDemoPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
