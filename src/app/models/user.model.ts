export class UserModel {
    id: number;
    createdAt: number;
    firstname: string = 'Sanjay';
    lastname: string = 'Singh';
    username: string;
    password: string;
    status: number;
}
