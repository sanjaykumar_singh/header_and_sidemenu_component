import { Component, Input } from '@angular/core';
import { UserModel } from '../../models/user.model';

@Component({
  selector: 'app-header',
  styleUrls: ['./app-header.component.css'],
  templateUrl: './app-header.component.html'
})
export class AppHeaderComponent {

  private currentSkin = "skin-blue";
  private user: UserModel = new UserModel();

  constructor() {
  }
}
